<?php 
/*** Plantila para el CTA en Home ***/
$placeholder_start = get_option('placeholder-start');
$url_start = get_option('url-start');
$txtbtn_start = get_option('txtbtn-start');
$startimg = get_option( 'startimg', '/img/KippGo-Kippero.jpg' );

?>

<div class="row home-start col-md-12 p-0 m-0" style="background-image: url(<?= $startimg; ?>);">

    <div class="row pt-3 pb-3 col-lg-12 col-12 p-lg-0 m-lg-0 p-0 m-0 ">

        <div class="input-group col-lg-6 col-12 p-0 m-0 mx-auto">
            <div class="col-12 col-lg-9 pl-lg-0 m-lg-0">
                <select name="ubicacion" id="ubicacion" class="form-control input-solicitar">
                    <option value="" disabled selected><?= $placeholder_start?></option>
                    <option value="biciexpressccs">Caracas</option>
                    <option value="cicloviasmcbo">Maracaibo</option>
                    <option value="biciexpressbsas">Buenos Aires</option>
                </select>
            </div>
 
            <div class="input-group-append col-12 col-lg-3 p-lg-0 m-lg-0">

                <!--a href="<-?=$url_start?>" target="_blank" class="col-12 p-0 m-0 col-lg-12" id="url_start" name="url_start"-->
                
                    <button class="btn btn-dark btn-solicitar font-weight-bold col-12" type="button" 
                        id="link-ubicacion" name="link-ubicacion" onClick="solicitardesp();">
                            <?= $txtbtn_start?> 
                    </button>
                <!--/a-->

            </div> 

        </div>
    </div>
</div>
<script>
    function solicitardesp(){
        var select = document.querySelector('#ubicacion');
        var url = '<?= $url_start?>';
        full = url + '/' + select.value;
        if (select.value == ''){
            alert('Por favor elija una ubicación...');
        }else {
             window.location.href = full;
        }
       
    }

</script>
