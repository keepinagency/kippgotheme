<?php 
/*** Plantilla para el Inino del Home ***/
$text_form_ini = get_option('text-form-ini');
$placeholder_name = get_option('placeholder-name');
$placeholder_tlf = get_option('placeholder-tlf');
$placeholder_mail = get_option('placeholder-mail');
$iniimg = get_option( 'iniimg', '/img/KippGo-Kippero.jpg' );
?>

<div class="row home-ini col-md-12 p-2 m-0 d-flex justify-content-center" style="background-image: url(<?= $iniimg; ?>);">

    <div class="col-lg-6 d-none d-lg-block"></div> 

    <div class="div-form col-lg-4 my-lg-5 py-4 my-2 col-11">

        <div class="text-center texto-form">
            <h5 class=""><?= $text_form_ini?></h5>
        </div>

        <form method="post" action="<?php echo get_template_directory_uri().'/temp_parts/correo_ini.php';?>" class="text-center" autocomplete="off">

            <div class="form-group p-0 m-0">
                <label for="name"></label>
                <input type="text" class="form-control ini" placeholder="<?= $placeholder_name?>" id="name" name="name" onkeypress="validarName();" required>
            </div>
            <div class="form-group p-0 m-0">
                <label for="phone"></label>
                <input type="tel" pattern="[0-9]{4}[0-9]{7}" class="form-control ini masked" placeholder="<?=$placeholder_tlf?>" id="phone" name="phone" onkeypress="validaPhone(event);" required>
            </div>
            <div class="form-group pb-3 m-0">
                <label for="email"></label>
                <input type="email" class="form-control ini" placeholder="<?=$placeholder_mail?>" id="email" name="email" required>
            </div>
            <button id="btn-form" type="submit" class="btn btn-naranja" onclick="validarForm();">Enviar</button>
        </form>
   </div>
   <div class="col-lg-2 d-none d-lg-block"></div> 
</div>

<script>
    function validarName(){
        if ((event.keyCode != 32) && (event.keyCode < 65) || (event.keyCode > 90) && (event.keyCode < 97) || (event.keyCode > 122))
            event.returnValue = false;
    }
    function validaPhone(evt){
        var code = (evt.which) ? evt.which : evt.keyCode;
        if(code==8) { // borrar.
        return true;
        } else if(code>=48 && code<=57 || code == 45 || code == 46) { // número y guión.
            event.returnValue = true;
        } else{ // otras teclas.
            event.returnValue = false;
        }
    }
    function validarForm(){
        var name = document.getElementById("name").value;
        var phone = document.getElementById("phone").value;
        var email = document.getElementById("email").value;
        if(!name || !phone || !email){
            //div = document.getElementById('textoError');
            div.style.display = '';
            event.returnValue = false;
        }else{
            //div = document.getElementById('textoError');
            div.style.display = 'none';
        }
    }    
</script>