<?php 
/*** Esta es la plantilla para el home ***/    
$nuevo_arreglo = new WP_Query(array(
	'post_type'         => 'post', 
    'category_name'     => 'about',
	'posts_per_page'    => 1
));
?>
<div class="row col-12 pl-lg-5 pr-lg-5 pt-lg-5 pt-3 pl-0 pr-0 m-0 d-flex-lg justify-content-lg-center">

    <?php if ($nuevo_arreglo->have_posts()) :?>
            <?php while ($nuevo_arreglo->have_posts()) : $nuevo_arreglo->the_post();?>
                
                <!--div class="col-lg-6 text-lg-right pr-lg-2 
                            col-12 pb-1 pl-0 pr-0 m-0 text-center about-img">
                    <?php echo the_post_thumbnail('medium');?>
                </div-->  
                <div class="col-lg-12 d-lg-flex justify-content-lg-center align-self-lg-center 
                            col-12 pt-1 pr-0 pl-0 m-0 text-center ">
                    <h1><a class="about-title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                </div>
            <?php endwhile;?>
    <?php endif;?>
    <div class="col-12 about-linea mb-lg-3 mt-lg-5 mb-2 mt-2"></div>
    
</div>


