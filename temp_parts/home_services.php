<?php 
/*** Esta es la plantilla para el home ***/  
$paginas = get_query_var('paged');  
$nuevo_arreglo = new WP_Query(array(
	'post_type'         => 'post', 
    'category_name'     => 'services',
    'orderby'           => 'modified',
    'order'             => 'ASC',
    'paged'             => $paginas,
	'posts_per_page'    => 3
));    
?>
<div class="row col-12 p-0 m-0 d-flex-lg justify-content-lg-center pb-lg-3 mt-lg-2">

    <?php if ($nuevo_arreglo->have_posts()) :?>
        <!--div class="col-12 text-center services-titulo"><h1>¿Qué ofrecemos?</h1></div-->

        <?php while ($nuevo_arreglo->have_posts()) : $nuevo_arreglo->the_post();?>
            
                <div class="col-12 p-0 mt-1 mb-1 col-lg-3 m-lg-4 text-left">

                    <a class="" href="<?php the_permalink(); ?>">
                        <div class="services-img" style="background-image: url('<?php echo get_the_post_thumbnail_url('');?>'); "></div>
                    </a>

                    <a class="services-title-a text-decoration-none" href="<?php the_permalink(); ?>">
                        <div class="services-title text-decoration-none text-center"><?php the_title(); ?></div>
                    </a>
                        <div class="services-post p-2 text-center"><?php the_excerpt(); ?></div>
                </div>
        <?php endwhile;?>
		</div>
        <!--div class="cont_pag_numbers">
            <-?php 
                echo paginate_links(array(
                'total' => $nuevo_arreglo->max_num_pages
            ));
            ?>
        </div-->
        
    <?php endif;?>
</div>
