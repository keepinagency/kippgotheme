<?php 
/*** Plantilla CARACTERISTICAS para el Home ***/    
$nuevo_arreglo = new WP_Query(array(
	'post_type'         => 'post', 
    'category_name'     => 'features',
    'order'             => 'ASC',
	'posts_per_page'    => 6
));
$features_start = get_option('features-start');
?>
<div class="row col-12 pt-3 m-0 d-lg-flex justify-content-lg-center">

    <div class="col-12">

        <!--div class="feat col-12 d-flex justify-content-center py-2 m-0">
            <h1>Caracteristicas</h1>
        </div-->

        <div class="row col-12 d-flex justify-content-center py-2 px-0 m-0">
            <h3 class="feat-text"><?= $features_start?></h3>
        </div>
    </div>

    <!--div class="col-lg-6 d-flex justify-content-center border border-primary p-0 m-0">imagen colocada por personalizador</div-->

    <?php if ($nuevo_arreglo->have_posts()):?>

        <div class="row col-lg-10 col-12 py-2 px-0 m-0 d-lg-flex justify-content-lg-center">

            <?php while ($nuevo_arreglo->have_posts()) : $nuevo_arreglo->the_post();?>

                <div class="col-lg-6 py-1 px-0 col-12 p-0 m-0">

                    <div class="py-lg-1 px-lg-2 p-0 m-0">
                        <h4><?= the_title(); ?></h4>
                    </div>

                    <div class="feat-cont py-lg-0 pl-lg-2 pr-lg-5 text-justify p-0 m-0">
                        <?= the_excerpt();?>
                    </div>

                </div>

            <?php endwhile;?>

        </div>
       
    <?php endif;?>
    <!--div class="col-12 p-0 m-0 about-linea"></div-->
       
</div>
