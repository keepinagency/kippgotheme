<?php 
/*** Esta es la plantilla para el home ***/    
$nuevo_arreglo = new WP_Query(array(
	'post_type'=>'post', 
    'category_name' => 'blog',
	'posts_per_page'=>4
));
?>
<div class="row col-12 p-0 m-0 d-flex-lg justify-content-lg-center pb-lg-3">
    <div class="row p-0 m-0 d-flex-lg justify-content-lg-center">

        <?php if ($nuevo_arreglo->have_posts()) :?>
            <div class="col-12 text-center pt-3"><h1>Blog</h1></div>

            <?php while ($nuevo_arreglo->have_posts()) : $nuevo_arreglo->the_post();?>
                
                    <div class="col-12 p-0 mt-1 mb-1 col-lg-3 m-lg-0 p-lg-0 text-center">
                        <a class="blog-title-a text-decoration-none" href="<?php the_permalink(); ?>">
                            <div class="blog-title text-decoration-none"><?php the_title(); ?></div>
                        </a>

                        <a class="" href="<?php the_permalink(); ?>">
                            <div class="blog-img ml-lg-2" style="background-image: url('<?php echo get_the_post_thumbnail_url('');?>'); "></div>
                        </a>
                            <div class="blog-post p-2 ml-lg-2"><?php the_excerpt(); ?></div>
                    </div>
            <?php endwhile;?>
            </div>
            
        <?php endif;?>
    </div>
</div>
