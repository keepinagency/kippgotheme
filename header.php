<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php wp_head();?>
    </head>
    <body <?php body_class();?> class="p-0 m-0">
        <?php wp_body_open();?>
<div class="container-fluid p-0 m-0 h-100">

    <div class="header p-0 m-0 row">
        <nav class="navbar navbar-light col-12">
             <!--div class="col-lg-1 pl-lg-3 m-lg-0 col-2 pl-0 m-0">
                <button class="navbar-toggler bg-butn" type="button" data-toggle="collapse" 
                        data-target="#menukippgo" aria-controls="menukippgo" 
                        aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div-->
            <div class="col-lg-10 pl-lg-5 m-lg-0 d-flex-lg justify-content-lg-start col-10 d-flex justify-content-start p-0 m-0">
                <a class="navbar-brand m-0 p-0"  href="<?php echo get_home_url(); ?>">
                    <?php
                        $custom_logo_id = get_theme_mod( 'custom_logo' );
                        $custom_logo_url = wp_get_attachment_image_url( $custom_logo_id , 'full' );
                        echo '<img class="imglogo" src="' . esc_url( $custom_logo_url ) . '" alt="Logo ">';
                    ?>
                </a>
            </div>
            <div class="col-lg-2 text-lg-right col-2 d-flex justify-content-end p-0 m-0">
                <a class="navbar-brand m-0 p-0 sesion" href="https://kippgo.com/app/login" target="_blank">
                   Iniciar sesión
                </a>
            </div>
            <!--Area menu   area-menu   -->
            <!--div class="col-12 col-lg-4 m-0 p-0">         
                <-?php
                    wp_nav_menu( array(
                        'container'       => 'div',
                        'container_class' => 'collapse navbar-collapse',
                        'container_id'    => 'menukippgo',
                        'theme_location'  => 'header-menu',
                        'menu_class'      => 'nav flex-column header-menu',
                        'walker'          => new WP_Bootstrap_Navwalker())
                    );
                ?>
            </div-->
        </nav>
        <!--/div-->
    </div><!-- header -->