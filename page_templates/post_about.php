<?php
/*
Template Name:about
Template Post Type: post
Esta es la plantilla para el About
 */ 
get_header(); ?>
<div class="row page col-md-12 p-0 m-0">
        <?php if ( have_posts() ) { ?>
            <div class="page-title col-12 text-center col-lg-12 pb-lg-2 pt-lg-4">
                <h1><?php the_title(); ?></h1>
            </div>
        <?php
            while ( have_posts() ) : the_post();
                ?>
                <div class="row page-cont col-12 d-flex-lg justify-content-lg-center p-0 m-0">

                    <!--div class="page-img col-lg-12">
                        <?php the_post_thumbnail('medium');?>
                    </div-->
                    <div class="page-post col-12 text-justify col-lg-10 pr-lg-5 pl-lg-5 ">
                        <?php the_content(); ?>
                    </div>
                    <div class="page-imgmob col-12 text-center d-flex justify-content-center pb-3 m-0 pb-lg-3">
                        <?php the_post_thumbnail('medium');?>
                    </div>
                    
                </div><!-- /.blog-post -->
                <?php
            endwhile;
            } 
        ?>
    </div>
<!--Navegacion para cada entrada o post-->
<!--div class="navegacion_entradas">    
    <-?php 
        if ( is_singular( 'post' ) ) {
            the_post_navigation( array(
                'prev_text'          => __( '&nbsp;' ).'<span class="nav_post">%title</span>',
                'next_text'          => '<span class="nav_post">%title</span>'.__( '&nbsp;' ),
                'in_same_term'       => true,
                'screen_reader_text' => __( '&nbsp;' )
            ) );
        }
    ?>
</div-->
<?php get_footer();?>
