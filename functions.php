<?php

    /* Libreria bootstrap para Nav - Menu */
    require_once('class-wp-bootstrap-navwalker.php');

    /*Definicion de rutas TEMP_PARTS*/
    define( 'Kippgo_VERSION', '0.0.1' );
    define( 'Kippgo_TEMP_PARTS', trailingslashit( get_template_directory() ) . 'temp_parts/' );

    /*Soporte para Titlos, Imagen destacada y logo*/
    function kippgo_setup(){
        add_theme_support( 'title-tag' );
        add_theme_support( 'post-thumbnails' );
        add_theme_support( 'custom-logo', array(
            'height'      => 100,
            'width'       => 400,
            'flex-height' => false,
            'flex-width'  => false,
            'header-text' => array( 'site-title', 'site-description'),
        ));
    }

    /*Archivos JS y CSS*/
    function kippgo_cssjs(){
        wp_enqueue_style('bootstrap_4', get_template_directory_uri() . '/css/bootstrap.min.css');
        wp_enqueue_script('bootstrap_js_4', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '', true);
        $dependencies = array('bootstrap_4');
        wp_enqueue_style( 'keepin-style', get_stylesheet_uri(), $dependencies );
    }

    /*Registro Menú Header and Footer*/
    function kippgo_register_menu() {
        register_nav_menu( 'header-menu', __('Header Menu'));
        register_nav_menu( 'footer-menu', __('Footer Menu'));
    }
    /****************FUNCION PARA EXTRACTO***********************/
    /*function custom_excerpt_length( $length){
        return 120;
    }*/

    
    /*Funciónes para el personalizador*
    ***********************************/
    function custom_kippgo_register($wp_customize){

        /***** AGREGO PANEL DE OPCIONES *****/
        /***********************************/
        $wp_customize->add_panel('kippgo', array(
            'title'         => 'kippGo Home',
            'descriptions'  => 'Opciones para el personalizador',
            'priority'      => 1,
        ));
        /**** AGREGO SECCION AL PANEL "KIPPGO"*****/ 

        /******************* SECCION HOMEPAGE **********************/
        /***********************************************************/
        $wp_customize->add_section('homeform', array(
            'title'         => __('Home - Formulario', 'textdomain'),
            'panel'         => 'kippgo',
            'priority'      => 1,
        ));
        /**** AGREGO SETTING Y CONTROL A LA SECCION "HOMEFORM" *****/
        /**********************************************************/

        /***** SETTING IMAGEN INICIO ********/
        /***********************************/
        $wp_customize->add_setting( 'iniimg', array (
            'default'        => get_template_directory_uri() . '/img/KippGo-Kippero.jpg',
            'capability'     => 'edit_theme_options',
            'type'           => 'option',
        ));

        $wp_customize->add_control( new WP_Customize_Image_Control($wp_customize, 'ini', array(
            'label'      => __( 'Imagen inicio', 'textdomain' ),
            'section'    => 'homeform',
            'settings'   => 'iniimg',
            'priority'   => 1,
        )));

        /****** SETTING ENCABEZADO FORM *****/
        /***********************************/
        $wp_customize->add_setting( 'text-form-ini', array(
            'type' => 'option',
            'capability' => 'edit_theme_options',
        ));
        $wp_customize->add_control('text-form-ini', array(
            'label' => __( 'Encabezado formulario', 'textdomain' ),
            'section' => 'homeform',
            'priority' => 2,
            'type' => 'text',
        ));
        /******** SETTING PLACEHOLDER NAME *********/
        /*******************************************/
        $wp_customize->add_setting( 'placeholder-name', array(
            'type' => 'option',
            'capability' => 'edit_theme_options',
        ));
        $wp_customize->add_control('placeholder-name', array(
            'label' => __( 'Texto placholder name', 'textdomain' ),
            'section' => 'homeform',
            'priority' => 3,
            'type' => 'text',
        ));
         /******** SETTING PLACEHOLDER TELF *********/
        /*******************************************/
        $wp_customize->add_setting( 'placeholder-tlf', array(
            'type' => 'option',
            'capability' => 'edit_theme_options',
        ));
        $wp_customize->add_control('placeholder-tlf', array(
            'label' => __( 'Texto placholder tlf', 'textdomain' ),
            'section' => 'homeform',
            'priority' => 4,
            'type' => 'text',
        ));
        
         /******** SETTING PLACEHOLDER MAIL *********/
        /*******************************************/
        $wp_customize->add_setting( 'placeholder-mail', array(
            'type' => 'option',
            'capability' => 'edit_theme_options',
        ));
        $wp_customize->add_control('placeholder-mail', array(
            'label' => __( 'Texto placholder-mail', 'textdomain' ),
            'section' => 'homeform',
            'priority' => 5,
            'type' => 'text',
        ));

        /****************** SECCION HOMEFEATURES ********************/
        /***********************************************************/
        $wp_customize->add_section('homefeatures', array(
            'title'         => __('Home - Caracteristicas', 'textdomain'),
            'panel'         => 'kippgo',
            'priority'      => 1,
        ));
         /******** SETTING CARACTERISTICAS HOME *********/
        /***********************************************/
        $wp_customize->add_setting( 'features-start', array(
            'type' => 'option',
            'capability' => 'edit_theme_options',
        ));
        $wp_customize->add_control('features-start', array(
            'label' => __( 'Titulo caracteristicas', 'textdomain' ),
            'section' => 'homefeatures',
            'priority' => 1,
            'type' => 'text',
        ));

        /****************** SECCION HOMECTA ********************/
        /***********************************************************/
        $wp_customize->add_section('homecta', array(
            'title'         => __('Home - Ubicación', 'textdomain'),
            'panel'         => 'kippgo',
            'priority'      => 1,
        ));

         /******** SETTING PLACEHOLDER START *********/
        /*******************************************/
        $wp_customize->add_setting( 'placeholder-start', array(
            'type' => 'option',
            'capability' => 'edit_theme_options',
        ));
        $wp_customize->add_control('placeholder-start', array(
            'label' => __( 'Texto placholder start', 'textdomain' ),
            'section' => 'homecta',
            'priority' => 4,
            'type' => 'text',
        ));
        
        /******** SETTING URL BTN START *********/
        /***************************************/
        $wp_customize->add_setting('url-start', array(
            'type'          => 'option',
            'capability'    => 'edit_theme_options',
            'default'       => '',
        ));

        $wp_customize->add_control('url-start', array(
            'label'         => __('URL CTA boton start', 'textdomain'),
            'section'       => 'homecta',
            'priority'      => 5,
            'type'          => 'text',
        ));

        /******** SETTING CTA START *********/
        /***********************************/
        $wp_customize->add_setting( 'txtbtn-start', array(
            'type' => 'option',
            'capability' => 'edit_theme_options',
        ));
        $wp_customize->add_control('txtbtn-start', array(
            'label' => __( 'Texto boton start', 'textdomain' ),
            'section' => 'homecta',
            'priority' => 6,
            'type' => 'text',
        ));

        /****** SETTING IMAGEN START ********/
        /***********************************/
        $wp_customize->add_setting( 'startimg', array (
            'default'        => get_template_directory_uri() . '/img/KippGo-Kippero.jpg',
            'capability'     => 'edit_theme_options',
            'type'           => 'option',
        ));

        $wp_customize->add_control( new WP_Customize_Image_Control($wp_customize, 'start', array(
            'label'      => __( 'Imagen Start', 'textdomain' ),
            'section'    => 'homecta',
            'settings'   => 'startimg',
            'priority'   => 7,
        )));
        

         /*********** SECCION SOCIAL MEDIA FOOTER ******************/
        /***********************************************************/
        $wp_customize->add_section('SocialMediaFooter', array(
            'title'         => __('Footer - Social Media ', 'textdomain'),
            'panel'         => 'kippgo',
            'priority'      => 2,
        ));

        /**** AGREGO SETTING Y CONTROL A LA SECCION "FOOTER" *******/
        /**********************************************************/

        /******* SETTING INSTA ICONO ********/
        /***********************************/
        $wp_customize->add_setting( 'instalogo', array (
            'default'        => get_template_directory_uri() . '/img/ico-instagram.png',
            'capability'     => 'edit_theme_options',
            'type'           => 'option',
        )); 

        $wp_customize->add_control( new WP_Customize_Image_Control($wp_customize, 'insta', array(
            'label'      => __( 'Icono Instagram', 'textdomain' ),
            'section'    => 'SocialMediaFooter',
            'settings'   => 'instalogo',
            'priority'   => 1,
        )));
        
        /******** SETTING INSTA URL *********/
        /***********************************/
        $wp_customize->add_setting( 'instaurl', array(
            'type' => 'option',
            'capability' => 'edit_theme_options',
        ));
        $wp_customize->add_control('instaurl', array(
            'label' => __( 'Perfil Instagram', 'textdomain' ),
            'section' => 'SocialMediaFooter',
            'priority' => 2,
            'type' => 'text',
        ));
        
        /******* SETTING FACE ICONO *********/
        /***********************************/
        $wp_customize->add_setting( 'facelogo', array (
            'default'        => get_template_directory_uri() . '/img/ico-facebook.png',
            'capability'     => 'edit_theme_options',
            'type'           => 'option',
        ));

        $wp_customize->add_control( new WP_Customize_Image_Control($wp_customize, 'face', array(
            'label'      => __( 'Icono Facebook', 'textdomain' ),
            'section'    => 'SocialMediaFooter',
            'settings'   => 'facelogo',
            'priority'   => 3,
        )));
        
        /******** SETTING FACE URL **********/
        /***********************************/
        $wp_customize->add_setting( 'faceurl', array(
            'type' => 'option',
            'capability' => 'edit_theme_options',
        ));
        $wp_customize->add_control('faceurl', array(
            'label' => __( 'Perfil Facebook', 'textdomain' ),
            'section' => 'SocialMediaFooter',
            'priority' => 4,
            'type' => 'text',
        ));

        /****** SETTING TWITTER ICONO *******/
        /***********************************/
        $wp_customize->add_setting( 'twitterlogo', array (
            'default'        => get_template_directory_uri() . '/img/ico-twitter.png',
            'capability'     => 'edit_theme_options',
            'type'           => 'option',
        ));

        $wp_customize->add_control( new WP_Customize_Image_Control($wp_customize, 'twitter', array(
            'label'      => __( 'Icono Twitter', 'textdomain' ),
            'section'    => 'SocialMediaFooter',
            'settings'   => 'twitterlogo',
            'priority'   => 5,
        )));
        
        /******* SETTING TWITTER URL ********/
        /***********************************/
        $wp_customize->add_setting( 'twitterurl', array(
            'type' => 'option',
            'capability' => 'edit_theme_options',
        ));
        $wp_customize->add_control('twitterurl', array(
            'label' => __( 'Perfil Twitter', 'textdomain' ),
            'section' => 'SocialMediaFooter',
            'priority' => 6,
            'type' => 'text',
        ));

    }

    /*Ejecución de acciones o funciones definidas*/
    add_action('wp_enqueue_scripts', 'kippgo_cssjs');
    add_action('after_setup_theme', 'kippgo_setup');
    add_action('after_setup_theme', 'kippgo_register_menu');
    add_action('customize_register' , 'custom_kippgo_register');

    //add_action( 'excerpt_length' , 'custom_excerpt_length', 999 );

    


?>
